from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

from .models import Estudiante, Empresa, Convocatoria, Postulacion, Configuracion
 
def PaginaInicio(request):
    return render(request,'app/index-PaginaInicio.html')

def post_login(request):
    usuario = request.POST['usuario']
    contraseña = request.POST['clave']

    u= authenticate(username=usuario, password= contraseña)

    if u is None:
        print('claves incorrectas')
        return HttpResponse('perros jijijue')
    else:
        login(request, u)
        if request.user.is_superuser:
	        return redirect ('app:Coordinador')
        else:
            if request.user.empresas:
                return redirect ('app:Empresa')
            else:
                return redirect ('app:Estudiante')

def post_logout(request):
    logout(request)
    return redirect('app:PaginaInicio')
    


def ActualizarSemestre(request):

    return render(request,'app/Actualizar-S.html')

def post_ActualizarSemestre(request):
    semestre = request.POST['semester']

    nuevosemestre = Configuracion(id=1)
    nuevosemestre.semestre=semestre

    nuevosemestre.save() 

    return redirect('app:Coordinador')

    
def laconvocatoria(request, id):
    convocatoria = Convocatoria.objects.get(id=id)
       
    contexto={
        'convocatoria': convocatoria
    }
    return render(request,'app/Convocatoria-especifica.html', contexto)

def cerrarlaconvocatoria(request, id):
    cerrada= Convocatoria.objects.get(id=id)
    cerrada.abierta = (False)
    cerrada.save()
    return redirect('app:listaConvocatoriasespecificas')

def Coordinador(request):
    if request.user.is_superuser:
	    return render(request,'app/Coordinador.html')
    else:
        return HttpResponse('perrojijueputa')
    

def crearConvocatoria(request):
    return render(request,'app/Crear-Convocatoria.html')

def post_crearConvocatoria(request):
    nombre = request.POST['nombre']
    fechaini = request.POST['fechaI']
    fechaCi = request.POST['fechaC']
    descripcion = request.POST['des']
    
    nuevaConv= Convocatoria()
    nuevaConv.nombre=nombre
    nuevaConv.fecha_inicial=fechaini
    nuevaConv.fecha_final= fechaCi
    nuevaConv.descripcion= descripcion
    nuevaConv.empresa_id=(17)
    
    nuevaConv.save()

    return redirect('app:listaConvocatoriasespecificas')

def editarConvocatoria(request, id):
    aidi=id
    contexto={
        'identificador' : aidi
    }
    return render(request,'app/EditarConvocatoria.html', contexto)

def post_editarConvocatoria(request, id):
    edicion= Convocatoria.objects.get(id=id)
    edicion.nombre = request.POST['nombre']
    edicion.fecha_inicial = request.POST['fechaI']
    edicion.fecha_final = request.POST['fechaC']
    
    edicion.save() 

    return redirect('app:laconvocatoria', id)

def crearEmpresa(request):
    return render(request,'app/crear-empresa.html')

def post_crearEmpresa(request):
    razon = request.POST['razonsocial']
    usuario = request.POST['usuario']
    contraseña = request.POST['contraseña']
    ciudad = request.POST['ciudad']
    contacto =request.POST['contacto']
    correo =request.POST['correo']

    nuevaempresa = User()
    empresanueva= Empresa()
    empresanueva.razon_social=razon
    empresanueva.ciudad=ciudad
    empresanueva.nombre_contacto=contacto
    nuevaempresa.username= usuario
    nuevaempresa.set_password(contraseña)
    nuevaempresa.email = correo
    #nuevaempresa.fecha_creacion = (2020/10/16)   

    nuevaempresa.save()

    empresanueva.usuarios_id = nuevaempresa.id
  
    empresanueva.save()

    return redirect('app:Empresa')

def crearEstudiante(request):

    return render(request, 'app/crear-estudiante.html')


def post_crearEstudiante (request):

    usuario = request.POST['usuario']
    contraseña = request.POST['contraseña']
    nombres = request.POST['nombres']
    apellidos =request.POST['apellidos']
    correo =request.POST['correo']

    nuevoestudiante = User()
    nuevoestudiante.username= usuario
    nuevoestudiante.set_password= contraseña
    nuevoestudiante.first_name = nombres
    nuevoestudiante.last_name = apellidos
    nuevoestudiante.email = correo

    nuevoestudiante.save() 
    
    estudiante= Estudiante()
    estudiante.usuarios_id = nuevoestudiante.id
    estudiante.save()

    return redirect('app:listadoEstudiantes')

def lEmpresa(request):
    # e= Empresa.objects.all()
    
    # contexto={
    #     'nombre': e
    # }
    return render(request,'app/Empresa.html')

def Error(request):
    return render(request,'app/Error.html')

def lEstudiante(request):
    return render(request,'app/Estudiante.html')

def listaConvocatoriasespecificas(request):
    convo = Convocatoria.objects.filter(empresa_id=11)

    contexto={
        'convocatoria': convo
    }
    return render(request,'app/Lista-Convocatorias-especificas.html',contexto)

def listaConvocatoriasEstudiantes(request):
    return render(request,'app/lista-convocatorias-estudiantes.html')

def listaConvocatorias(request):
    convocatorias = Convocatoria.objects.all()
 
    contexto={
        'convocatorias': convocatorias,
        
    }
    return render(request,'app/Lista-Convocatorias.html',contexto)

def listaEmpresas(request):
    empresa= Empresa.objects.all()

    contexto={
        'empresas': empresa
    }
    return render(request,'app/Lista-empresas.html',contexto)

def listaPostulaciones(request):
    return render(request,'app/Lista-postulaciones.html')

def listadoEstudiantes(request):

    estudiantes = Estudiante.objects.all()
        
    contexto={
        'estudiantes': estudiantes
    }

    return render(request,'app/Listado-estudiantes.html',contexto)

def listadoEstuadiantesenPracticas(request):
    return render(request,'app/Listado-Pestudiantes.html')

def perfilEmpresa(request, id):

    datos = Empresa.objects.get(id=id)

    contexto={
        'empresa':datos
    }

    return render(request,'app/Perfil-empresa.html', contexto)

def perfilEstudiante(request, id):
    estudiante = Estudiante.objects.get(id=id)

    contexto={
        'estudiante': estudiante
    }
    return render(request,'app/Perfil-estudiante.html',contexto)

def postulacionespecifica(request):
    return render(request,'app/Postulacion-especifica.html')
