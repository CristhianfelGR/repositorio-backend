from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Estudiante (models.Model):
    fortalezas = models.CharField(max_length=45)
    herramientas = models.CharField(max_length=45)

    usuarios = models.ForeignKey(
        User,
        related_name='estudiantes',
        on_delete= models.PROTECT
    )

    class Meta:
        app_label: 'app'

class Coordinador (models.Model):
    celular = models.CharField(max_length=45)
    
    usuarios = models.ForeignKey(
        User,
        related_name='coordinadores',
        on_delete= models.PROTECT
    )

    class Meta:
        app_label: 'app'

class Empresa (models.Model):
    razon_social = models.CharField(max_length=45)
    ciudad = models.CharField(max_length=45)
    nombre_contacto = models.CharField(max_length=45)
    fecha_creacion = models.DateField(auto_now=False, auto_now_add=True)
    
    usuarios = models.ForeignKey(
        User,
        related_name='empresas',
        on_delete= models.PROTECT
    )

    class Meta:
        app_label: 'app'

class Convocatoria (models.Model):
    nombre = models.CharField(max_length=45)
    descripcion = models.CharField(max_length=45)
    fecha_inicial = models.DateField(auto_now=False, auto_now_add=False)
    fecha_final = models.DateField(auto_now=False, auto_now_add=False)
    abierta = models.BooleanField(default= True)
    fecha_creacion = models.DateField(auto_now=True, auto_now_add=False)
    
    empresa = models.ForeignKey(
        Empresa,
        related_name='convocatorias',
        on_delete= models.PROTECT
    )
        
    class Meta:
        app_label: 'app'

class Postulacion (models.Model):
    id_estudiante = models.IntegerField()
    id_convocatoria = models.IntegerField()
    semestre = models.CharField(max_length=45)
    aceptada = models.BooleanField(null=True)
    fecha_creacion = models.DateField(auto_now=True, auto_now_add=False)
    
    
    convocatoria = models.ForeignKey(
        Convocatoria,
        related_name='postulaciones',
        on_delete= models.PROTECT
    )

    estudiante = models.ForeignKey(
        Estudiante,
        related_name='postulaciones',
        on_delete= models.PROTECT
    )
        
    class Meta:
        app_label: 'app'


class Configuracion (models.Model):
    
    semestre = models.CharField(max_length=45)

    class Meta:
        app_label: 'app'