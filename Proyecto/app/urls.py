from django.urls import path 
from . import views 
 
app_name = 'app' 
urlpatterns = [
    path('', views.PaginaInicio, name='PaginaInicio'),
    path('post_login/', views.post_login, name='post_login'),
    path('post_logout/', views.post_logout, name='post_logout'),
    

    path('ActualizarSemestre/', views.ActualizarSemestre, name='ActualizarSemestre'),
    path('post_ActualizarSemestre/', views.post_ActualizarSemestre, name='post_ActualizarSemestre'),

    path('laconvocatoria/<int:id>/', views.laconvocatoria, name='laconvocatoria'),
    path('cerrarlaconvocatoria/<int:id>/', views.cerrarlaconvocatoria, name='cerrarlaconvocatoria'),


    path('Coordinador/', views.Coordinador, name='Coordinador'),

    path('crearConvocatoria/', views.crearConvocatoria, name='crearConvocatoria'),
    path('post_crearConvocatoria/', views.post_crearConvocatoria, name='post_crearConvocatoria'),
    
    path('editarConvocatoria/<int:id>/', views.editarConvocatoria, name='editarConvocatoria'),
    path('post_editarConvocatoria/<int:id>/', views.post_editarConvocatoria, name='post_editarConvocatoria'),

    path('crearEmpresa/', views.crearEmpresa, name='crearEmpresa'),
    path('post_crearEmpresa/', views.post_crearEmpresa, name='post_crearEmpresa'),

    path('crearEstudiante/', views.crearEstudiante, name='crearEstudiante'),
    path('post_crearEstudiante/', views.post_crearEstudiante, name='post_crearEstudiante'),

    path('Empresa/', views.lEmpresa, name='Empresa'),
    path('Error/', views.Error, name='Error'),
    path('Estudiante/', views.lEstudiante, name='Estudiante'),
    path('listaConvocatoriasespecificas/', views.listaConvocatoriasespecificas, name='listaConvocatoriasespecificas'),
    path('listaConvocatoriasEstudiantes/', views.listaConvocatoriasEstudiantes, name='listaConvocatoriasEstudiantes'),
    path('listaConvocatorias/', views.listaConvocatorias, name='listaConvocatorias'),
    path('listaEmpresas/', views.listaEmpresas, name='listaEmpresas'),
    path('listaPostulaciones/', views.listaPostulaciones, name='listaPostulaciones'),

    
    path('listadoEstudiantes/', views.listadoEstudiantes, name='listadoEstudiantes'),

    path('listadoEstuadiantesenPracticas/', views.listadoEstuadiantesenPracticas, name='listadoEstuadiantesenPracticas'),
    path('perfilEmpresa/<int:id>/', views.perfilEmpresa, name='perfilEmpresa'),

    path('perfilEstudiante/<int:id>/', views.perfilEstudiante, name='perfilEstudiante'),
    
    path('postulacionespecifica/', views.postulacionespecifica, name='postulacionespecifica'),
]
