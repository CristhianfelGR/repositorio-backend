from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Categoria, Pelicula
 
#Lista de peliculas
# lista=['Avatar','Titanic', 'Constantine', 'Star Wars']

# lista= [
#     {'id':0,'nombre': 'Pulp Fiction', 'year':1998},
#     {'id':1,'nombre': 'The Hateful Eight', 'year':2015},
#     {'id':2,'nombre': 'Django Unchained', 'year':2013},
#     {'id':3,'nombre': 'Kill Bill Vol.1', 'year':2004},
# ]

def index(request):
    return render(request,'app/index.html')

def acercade(request):
    return render(request,'app/acercade.html')

def peliculas(request):
    #obtiene las peliculas
    lista= Pelicula.objects.all().order_by('anio')
    contexto = {
        # 'lista_peliculas': lista,
        # 'year': 2021,
        # 'tienda': 'Blockbuster'
        'peliculas': lista

    }
    return render(request,'app/peliculas.html',contexto)

def pelicula(request, id):

#     titulo=''
#     if id ==1:
#         titulo='Avatar'
#     elif id==2:
#         titulo='Titanic'
#     elif id==3:
#         titulo='Constantine'
#     else:
#         titulo='Desconocido'
#     #Definición del contexto
#     contexto={
#         'titulo':titulo
#    }

    # #Definición del contexto
    # contexto ={}
    
    # #Verifica que el id esté dentro de la lista
    # if id < len(lista):
    #     contexto['pelicula']=lista[id]
    
    ##Ahora con models
    obj_pelicula= Pelicula.objects.get(id=id)
    contexto= {
        'pelicula': obj_pelicula
    }
    return render(request,'app/pelicula.html',contexto)

def categorias(request):
    #Obtiene todas las caegorias
    lista = Categoria.objects.all()
    #Crea el contexto
    contexto = {
        'categorias': lista
    }
    # Invoca el template enviando el contexto 
    return render(request,'app/categoria.html', contexto)

def form_crear_categoria(request):
        
    return render(request,'app/formCrearCategoria.html')

def post_crear_categoria(request):
    #Obtiene el nombre de la categoria
    nombre= request.POST['nombre']
    #Crea el Objeto
    cat=Categoria(nombre=nombre)
    #Guarda el objeto en la bd
    cat.save()
    #Redirecciona a la página categorias
    return redirect('app:categorias')

def form_crear_pelicula(request):
    #obtiene las categorias
    lista_categorias=Categoria.objects.all().order_by('nombre')
    #Crea el conexto
    contexto={
        'categorias': lista_categorias
    }
    #muestra el template con e contexto
    return render(request, 'app/formCrearPelicula.html',contexto)

def post_crear_pelicula(request):
    #Obtiene los datos del formulario
    titulo =request.POST['titulo']
    anio=request.POST['anio']
    sinopsis=request.POST['sinopsis']
    actores=request.POST['actores']
    id_categoria=int(request.POST['categoria'])

    #obtiene la categoria
    categoria=Categoria.objects.get(pk=id_categoria)

    #Crea el objeto Pelicula
    nueva_pelicula= Pelicula()
    nueva_pelicula.titulo=titulo
    nueva_pelicula.anio=anio
    nueva_pelicula.sinopsis=sinopsis
    nueva_pelicula.actores=actores
    nueva_pelicula.categoria=categoria

    #Guarda el objeto en la base de datos
    nueva_pelicula.save()
    
    #Redirecciona a la página de películas
    return redirect('app:peliculas')



    