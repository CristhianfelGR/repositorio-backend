	
from django.urls import path 
from . import views 
 
app_name = 'app' 
urlpatterns = [
    path('', views.index, name='index'), 
    path('peliculas/', views.peliculas,name='peliculas'),
    path('pelicula/<int:id>/', views.pelicula, name='pelicula'),
    path('pelicula/crear', views.form_crear_pelicula, name='form_crear_pelicula'),
    path('pelicula/crear_post', views.post_crear_pelicula, name='post_crear_pelicula'),

    path('categorias/', views.categorias,name='categorias'),
    path('categorias/crear', views.form_crear_categoria, name='form_crear_categoria'),
    path('categorias/crear_post', views.post_crear_categoria, name='post_crear_categoria'),
    
    path('acercade/', views.acercade,name='acercade'),

]